
this.Documents= new Mongo.Collection("documents");
EditingUsers= new Mongo.Collection("editingUsers");

Meteor.startup(() => {
  // code to run on server at startup
  if(!Documents.findOne()){ //continua si no encuentra uno
  	Documents.insert({title:"my nuevo documentos"});
  }
});

Meteor.methods({
	addEditingUser:function(){
		var doc,user,eusers;
		doc=Documents.findOne();
		if(!doc){return;} // no se encontro un documento
		if(!this.userId){return;} // no se encontro un suer log in
		// ahora tengo un user y un doc
		user=Meteor.user().profile;
		eusers=EditingUsers.findOne({docid:doc._id});
		if(!eusers){
			eusers={
					docid:doc._id,
					users:{},
			};
		}

		//user.lastEdit=new Date();

		eusers.users[this.userId]=user;

		EditingUsers.upsert({_id:eusers._id},eusers);//upsert actualiza los que tenga igual id
	}
})

